import creep.mission.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass

private val module = SerializersModule {
    polymorphic(CreepMission::class) {
        subclass(NoMission.serializer())
        subclass(HarvestMission.serializer())
        subclass(UpgradeMission.serializer())
        subclass(BuildMission.serializer())
        subclass(DeliverMission.serializer())
    }
}

val serializer = Json { serializersModule = module }