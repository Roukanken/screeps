import creep.mission.*
import creep.state
import creep.mission.selectMission
import creep.spawnCreeps
import screeps.api.*
import screeps.utils.isEmpty
import screeps.utils.unsafe.delete

/**
 * Entry point
 * is called by screeps
 *
 * must not be removed by DCE
 */
@Suppress("unused")
fun loop() {

    cleanMemory()
    spawnCreeps(Game.spawns.values.first())

    for ((_, creep) in Game.creeps) {
        creep.memory.state = creep.memory.state.execute(creep)
    }

    val controller = Game.spawns.values.first().room.controller
    if (controller != null && controller.level >= 2) {
        when (controller.room.find(FIND_MY_STRUCTURES).count { it.structureType == STRUCTURE_EXTENSION }) {
            0 -> controller.room.createConstructionSite(29, 27, STRUCTURE_EXTENSION)
            1 -> controller.room.createConstructionSite(28, 27, STRUCTURE_EXTENSION)
            2 -> controller.room.createConstructionSite(27, 27, STRUCTURE_EXTENSION)
            3 -> controller.room.createConstructionSite(26, 27, STRUCTURE_EXTENSION)
            4 -> controller.room.createConstructionSite(25, 27, STRUCTURE_EXTENSION)
            5 -> controller.room.createConstructionSite(24, 27, STRUCTURE_EXTENSION)
            6 -> controller.room.createConstructionSite(23, 27, STRUCTURE_EXTENSION)
        }
    }

}

fun cleanMemory() {
    if (Game.creeps.isEmpty()) return

    for ((creepName, _) in Memory.creeps) {
        if (Game.creeps[creepName] == null) {
            console.log("deleting obsolete memory entry for creep $creepName")
            delete(Memory.creeps[creepName])
        }
    }
}