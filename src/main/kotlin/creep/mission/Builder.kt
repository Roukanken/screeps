package creep.mission

import creep.harvestSource
import creep.moveIfFar
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import screeps.api.Creep
import screeps.api.FIND_MY_CONSTRUCTION_SITES

@Serializable
enum class BuilderState {
    BUILDING,
    HARVESTING,
    FINISHED,
}

@Serializable
@SerialName("BUILD")
data class BuildMission(
    var status: BuilderState,
) : CreepMission() {
    override val missionType = MissionType.BUILD
    override fun execute(creep: Creep): CreepMission {
        status = when {
            status == BuilderState.HARVESTING && creep.store.getFreeCapacity() == 0 -> BuilderState.BUILDING
            status == BuilderState.BUILDING && creep.store.getUsedCapacity() == 0 -> BuilderState.FINISHED
            else -> status
        }

        when (status) {
            BuilderState.BUILDING -> {
                val target = creep.room.find(FIND_MY_CONSTRUCTION_SITES).firstOrNull()
                if (target != null) {
                    creep.moveIfFar(target) { build(target) }
                }
            }
            BuilderState.HARVESTING -> creep.harvestSource()
            BuilderState.FINISHED -> return NoMission
        }

        return this
    }
}