package creep.mission

import creep.state
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import screeps.api.*

@Serializable
abstract class CreepMission {
    abstract val missionType: MissionType
    abstract fun execute(creep: Creep): CreepMission
}

@Serializable
@SerialName("NONE")
object NoMission : CreepMission() {
    override val missionType = MissionType.NONE
    override fun execute(creep: Creep): CreepMission = creep.selectMission()
}

enum class MissionType {
    HARVEST,
    UPGRADE,
    NONE,
    BUILD,
    DELIVER,
}

fun Creep.selectMission(): CreepMission {
    val count = Game.creeps.values.map { it.memory.state }
        .groupingBy { it.missionType }
        .eachCount()

    val constructionSites = room.find(FIND_MY_CONSTRUCTION_SITES)
    return when {
        store.getUsedCapacity() == 0 -> HarvestMission
        room.freeStoreOwners().any() -> DeliverMission(target = room.freeStoreOwners().first())
        constructionSites.isNotEmpty() -> BuildMission(BuilderState.HARVESTING)
        else -> UpgradeMission(UpgradeState.HARVESTING)
//        else -> NoMission
    }
}

val STORE_OWNERS = listOf(STRUCTURE_EXTENSION, STRUCTURE_SPAWN, STRUCTURE_TOWER)
private fun Room.freeStoreOwners(): Iterable<StoreOwner> =
    find(FIND_MY_STRUCTURES)
        .filter { it.structureType in STORE_OWNERS }
        .map { it.unsafeCast<StoreOwner>() }
        .filter { it.store[RESOURCE_ENERGY] < it.store.getCapacity(RESOURCE_ENERGY) }
