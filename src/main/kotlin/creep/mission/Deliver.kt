package creep.mission

import creep.moveIfFar
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import screeps.api.*
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

@Serializable
@SerialName("DELIVER")
data class DeliverMission(
    private var targetId: String,
) : CreepMission() {

    constructor(target: StoreOwner) : this(targetId = target.id)

    val target by Pointer<StoreOwner>(this::targetId)

    override val missionType = MissionType.DELIVER
    override fun execute(creep: Creep): CreepMission = with(creep) {
        moveIfFar(target) { transfer(target, RESOURCE_ENERGY) }
        if (store.getUsedCapacity() > 0 && target.store.getFreeCapacity() > 0) this@DeliverMission else NoMission
    }

}

class Pointer<T : Identifiable>(val id: KMutableProperty0<String>) : ReadWriteProperty<Any, T> {
    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        return Game.getObjectById(id.get()) ?: error("Target not found")
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        id.set(value.id)
    }
}