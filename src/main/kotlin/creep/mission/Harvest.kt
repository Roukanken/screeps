package creep.mission

import creep.harvestSource
import creep.storeEnergy
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import screeps.api.Creep
import screeps.api.OK

@Serializable
@SerialName("HARVEST")
object HarvestMission : CreepMission() {
    override val missionType = MissionType.HARVEST
    override fun execute(creep: Creep): CreepMission {
        return if (creep.store.getFreeCapacity() > 0) {
            creep.harvestSource()
            this
        } else {
            NoMission
        }
    }
}
