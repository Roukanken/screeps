package creep.mission

import creep.harvestSource
import creep.moveIfFar
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import screeps.api.Creep

@Serializable
enum class UpgradeState {
    UPGRADING,
    HARVESTING,
    FINISHED,
}

@Serializable
@SerialName("UPGRADE")
data class UpgradeMission(
    var status: UpgradeState,
) : CreepMission() {
    override val missionType = MissionType.UPGRADE
    override fun execute(creep: Creep): CreepMission {
        status = when {
            status == UpgradeState.HARVESTING && creep.store.getFreeCapacity() == 0 -> UpgradeState.UPGRADING
            status == UpgradeState.UPGRADING && creep.store.getUsedCapacity() == 0 -> UpgradeState.FINISHED
            else -> status
        }

        when (status) {
            UpgradeState.HARVESTING -> creep.harvestSource()
            UpgradeState.UPGRADING -> creep.moveIfFar(creep.room.controller!!) { upgradeController(room.controller!!) }
            UpgradeState.FINISHED -> return NoMission
        }

        return this
    }

}
