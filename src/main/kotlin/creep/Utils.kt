package creep

import screeps.api.*

inline val BodyPartConstant.cost
    get() = BODYPART_COST[this]!!

inline val Array<BodyPartConstant>.cost
    get() = sumOf { it.cost }