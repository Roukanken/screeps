package creep

import screeps.api.*
import screeps.api.structures.StructureExtension
import screeps.api.structures.StructureSpawn

fun <T : HasPosition> nearest(findTargets: Creep.() -> Array<T>): (Creep.() -> T?) = {
    findTargets().minByOrNull { it.pos.getRangeTo(this) }
}

object Find {
    val sources: (Creep.() -> Array<Source>) = { room.find(FIND_SOURCES) }
    val spawns: (Creep.() -> Array<StructureSpawn>) = { room.find(FIND_MY_SPAWNS) }
    val constructionSites: (Creep.() -> Array<ConstructionSite>) = { room.find(FIND_MY_CONSTRUCTION_SITES) }
}

fun Creep.harvestSource(strategy: Creep.() -> Source? = nearest(Find.sources)) {
    val source = strategy() ?: error("${name}: Can't find source")

    moveIfFar(source) {
        harvest(source)
    }
}

fun Creep.storeEnergy(strategy: Creep.() -> StoreOwner? = nearest(Find.spawns)): ScreepsReturnCode {
    val target = strategy() ?: error("${name}: Can't target to store energy")

    return moveIfFar(target) {
        transfer(target, RESOURCE_ENERGY)
    }
}

fun Creep.moveIfFar(target: HasPosition, block: Creep.() -> ScreepsReturnCode): ScreepsReturnCode {
    val result = block()
    if (result == ERR_NOT_IN_RANGE) {
        moveTo(target)
        return ERR_NOT_IN_RANGE
    }
    return result
}