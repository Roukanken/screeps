package creep

import screeps.api.*
import screeps.api.structures.StructureSpawn

fun spawnCreeps(spawn: StructureSpawn) {
    val body = arrayOf<BodyPartConstant>(WORK, CARRY, MOVE)

    if (spawn.room.energyAvailable < body.cost) return

    if (Game.creeps.values.count() >= 5) {
        return
    }

    val name = "${Game.time}"
    val result = spawn.spawnCreep(body, name)

    when (result) {
        OK -> println("spawned $name with body $body")
        ERR_BUSY, ERR_NOT_ENOUGH_ENERGY -> run { } // do nothing
        else -> println("unhandled error code $result")
    }
}


