package creep

import creep.mission.CreepMission
import creep.mission.NoMission
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import screeps.api.CreepMemory
import screeps.utils.memory.memoryWithSerializer
import serializer

var CreepMemory.state by memoryWithSerializer(
    { NoMission },
    { data -> serializer.encodeToString(data) },
    { string -> serializer.decodeFromString<CreepMission>(string) },
)
